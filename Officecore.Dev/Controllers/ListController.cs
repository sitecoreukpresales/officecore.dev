﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Officecore.Dev.Configuration;
using Officecore.Dev.Configuration.SiteUI;
using Officecore.Dev.Configuration.SiteUI.Base;
using Officecore.Dev.Models;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace Officecore.Dev.Controllers
{
    public class ListController : OfficecoreBaseController
    {
        public ActionResult Carousel()
        {
            var slider = DataSourceItemOrCurrentItem;
            MultilistField slideritems = (MultilistField)slider.Fields["Source"];
            var sliders = slideritems.GetItems().ToList();
            IEnumerable<CarouselItem> items = sliders.Select(x => new CarouselItem(x)).Where(x => SiteConfiguration.DoesItemExistInCurrentLanguage(x.Item));
            return !items.IsNullOrEmpty() ? View(UIHelpers.GetPropertyValue("view", "Carousel"), items) : ShowListIsEmptyPageEditorAlert();
        }

        public ActionResult RecentItems()
        {
            IEnumerable<NewsItem> items = new NewsItem(DataSourceItemOrCurrentItem).ChildrenInCurrentLanguage;
            return !items.IsNullOrEmpty() ? View(UIHelpers.GetPropertyValue("view", "RecentItems"), items) : ShowListIsEmptyPageEditorAlert();
        }

        public ActionResult Clients()
        {
            IEnumerable<Clients> items = new Clients(DataSourceItemOrCurrentItem).ChildrenInCurrentLanguage;
            return !items.IsNullOrEmpty() ? View(UIHelpers.GetPropertyValue("view", "Clients"), items) : ShowListIsEmptyPageEditorAlert();
        }

        public ActionResult NewsList()
        {
            IEnumerable<NewsItem> items = new NewsItem(DataSourceItemOrCurrentItem).ChildrenInCurrentLanguage;
            return !items.IsNullOrEmpty() ? View(UIHelpers.GetPropertyValue("view", "NewsList"), items) : ShowListIsEmptyPageEditorAlert();
        }

        public ActionResult Selection()
        {
            IEnumerable<SimpleItem> items = new SimpleItem(DataSourceItemOrCurrentItem).ChildrenInCurrentLanguage;
            return !items.IsNullOrEmpty() ? View(UIHelpers.GetPropertyValue("view", "Selection"), items) : ShowListIsEmptyPageEditorAlert();
        }

        public ActionResult ItemList()
        {
            IEnumerable<SimpleItem> items = new SimpleItem(DataSourceItemOrCurrentItem).ChildrenInCurrentLanguage;
            return !items.IsNullOrEmpty() ? View(UIHelpers.GetPropertyValue("view", "ItemList"), items) : ShowListIsEmptyPageEditorAlert();
        }

        public ActionResult GalleryList()
        {
            var tagparent = UIHelpers.GetPropertyValue("tags", "");
            if (string.IsNullOrEmpty(tagparent))
            {
                ShowListIsEmptyPageEditorAlert();
            }
            GalleryItemList item = new GalleryItemList(DataSourceItemOrCurrentItem);

            item.TagParent = new ID(tagparent);
            return View(UIHelpers.GetPropertyValue("view", "GalleryList"), item);
        }

        public ActionResult Locations()
        {
            Item location = SiteConfiguration.GetLocationsItem();
            if (location != null)
            {
                IEnumerable<LocationItem> items = new LocationItem(DataSourceItemOrCurrentItem).ChildrenInCurrentLanguage;
                return !items.IsNullOrEmpty() ? View(UIHelpers.GetPropertyValue("view", "Locations"),  items) : ShowListIsEmptyPageEditorAlert();

            }
            return null;
        }

        //public ActionResult Cases()
        //{
        //    SFCases sfcase = new SFCases();
        //    var cases = sfcase.GetAllCases(Sitecore.Context.User.Profile.Email);
        //    return !cases.IsNullOrEmpty() ? View(cases) : ShowListIsEmptyPageEditorAlert();
        //}
        public ActionResult ReturnRequests()
        {
            IEnumerable<ReturnRequest> items = DataSourceItem.GetChildren().Select(x => new ReturnRequest(x)).Where(x => x.Email == Sitecore.Context.User.Profile.Email);
            return !items.IsNullOrEmpty() ? View(items.OrderByDescending(x => x.Created).Take(10)) : ShowListIsEmptyPageEditorAlert();
        }


        private ActionResult ShowListIsEmptyPageEditorAlert()
        {
            return IsPageEditorEditing ? View("ShowPageEditorAlert", new PageEditorAlert(PageEditorAlert.Alerts.ListIsEmpty)) : null;
        }

    }
}
