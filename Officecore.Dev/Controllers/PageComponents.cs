﻿using System.Web.Mvc;
using Officecore.Dev.Configuration.SiteUI;
using Officecore.Dev.Configuration.SiteUI.Base;
using Officecore.Dev.Models;

namespace Officecore.Dev.Controllers
{
    public class PageComponents : OfficecoreBaseController
    {
        public ActionResult Chart()
        {
            return !IsDataSourceItemNull ? View(UIHelpers.GetPropertyValue("view", "Chart"), new Chart(DataSourceItem)) : ShowNoDataSourcePageEditorAlert();
        }

        public ActionResult Tabs()
        {
            var item = DataSourceItemOrCurrentItem;
            return item !=null? View(UIHelpers.GetPropertyValue("view", "Tabs"), new Tabs(item)) : ShowNoDataSourcePageEditorAlert();
        }

        private ActionResult ShowNoDataSourcePageEditorAlert()
        {
            return IsPageEditorEditing ? View("ShowPageEditorAlert", new PageEditorAlert(PageEditorAlert.Alerts.DatasourceIsNull)) : null;
        }


    }
}