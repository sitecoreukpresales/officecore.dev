﻿using System.Web.Mvc;
using Officecore.Dev.Configuration.SiteUI;
using Officecore.Dev.Configuration.SiteUI.Base;
using Officecore.Dev.Models;

namespace Officecore.Dev.Controllers
{
    public class SingleItemController : OfficecoreBaseController
    {
        public ActionResult BlockQuote()
        {
            return !IsDataSourceItemNull ? View(UIHelpers.GetPropertyValue("view", "BlockQuote"), new BlockQuote(DataSourceItem)) : ShowNoDataSourcePageEditorAlert();
        }

        public ActionResult Banner()
        {
            return !IsDataSourceItemNull ? View(UIHelpers.GetPropertyValue("view", "Banner"), new CarouselItem(DataSourceItem)) : ShowNoDataSourcePageEditorAlert();
        }

        public ActionResult Steps()
        {
            return !IsDataSourceItemNull ? View(UIHelpers.GetPropertyValue("view", "Steps"), new SimpleItem(DataSourceItem)) : ShowNoDataSourcePageEditorAlert();
        }

        public ActionResult ServiceBlock()
        {
            return !IsDataSourceItemNull ? View(UIHelpers.GetPropertyValue("view", "ServiceBlock"), new SimpleItem(DataSourceItem)) : ShowNoDataSourcePageEditorAlert();
        }

        public ActionResult ImportantInfo()
        {
            return !IsDataSourceItemNull ? View(UIHelpers.GetPropertyValue("view", "ImportantInfo"), new SimpleItem(DataSourceItem)) : ShowNoDataSourcePageEditorAlert();
        }

        public ActionResult VideoBanner()
        {
            return !IsDataSourceItemNull ? View(UIHelpers.GetPropertyValue("view", "VideoBanner"), new Video(DataSourceItem)) : ShowNoDataSourcePageEditorAlert();
        }

        public ActionResult Location()
        {
            return View(UIHelpers.GetPropertyValue("view", "Location"), (new LocationItem(DataSourceItemOrCurrentItem)));
        }

        public ActionResult ProductSpot()
        {
            return !IsDataSourceItemNull ? View(UIHelpers.GetPropertyValue("view", "ProductSpot"), new SimpleItem(DataSourceItem)) : ShowNoDataSourcePageEditorAlert();
        }

        private ActionResult ShowNoDataSourcePageEditorAlert()
        {
            return IsPageEditorEditing ? View("ShowPageEditorAlert", new PageEditorAlert(PageEditorAlert.Alerts.DatasourceIsNull)) : null;
        }




    }
}
