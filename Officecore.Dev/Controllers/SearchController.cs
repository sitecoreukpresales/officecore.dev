﻿using System;
using System.Web.Mvc;
using Officecore.Dev.Configuration.SiteUI.Base;
using Officecore.Dev.Models;

namespace Officecore.Dev.Controllers
{
    public class SearchController : OfficecoreBaseController
    {
        [HttpGet]
        public ActionResult Search(string searchStr, string tag)
        {
            return View(new SearchResults("*", new string[] { String.Format("{0}|{1}", "Tags", tag) }));
        }

        [HttpPost]
        public ActionResult Search(string searchStr, string[] facets)
        {
            return View(new SearchResults(searchStr, facets));
        }
    }
}