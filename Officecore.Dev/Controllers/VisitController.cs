﻿using System.Web.Mvc;
using Officecore.Dev.Configuration.SiteUI.Base;
using Officecore.Dev.Models;

namespace Officecore.Dev.Controllers
{
    public class VisitController : OfficecoreBaseController
    {
        public ActionResult VisitDetails()
        {
            return View("Profiles", new VisitInformation()) ;
        }

        private ActionResult ShowEditorAlert()
        {
            return IsPageEditorEditing ? View("ShowPageEditorAlert", new PageEditorAlert(PageEditorAlert.Alerts.VisitDetailsNotAllowedInPageEditor)) : null;
        }
    }
}