﻿using System;
using System.Linq;
using System.Web.Mvc;
using Ninject;
using Ninject.Parameters;
using Officecore.Dev.Configuration.Helpers;
using Officecore.Dev.Configuration.SiteUI.Analytics;
using Officecore.Dev.Configuration.SiteUI.Base;
using Officecore.Dev.Configuration.SiteUI.Forms;
using Officecore.Dev.Models;
using Sitecore.Analytics;
using Sitecore.Social.Klout.Api.Domain;
using Sitecore.Social.Klout.Api.Domain.Model;

namespace Officecore.Dev.Controllers
{
    [Authorize]
    public class AccountController : OfficecoreBaseController
    {
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            if (!String.IsNullOrEmpty(Request.QueryString["authResult"]))
            {
                return RedirectToLocal(returnUrl);
            }

            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateFormHandler]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                string username = System.Web.Security.Membership.GetUserNameByEmail(model.UserName);
                if(string.IsNullOrEmpty(username))
                {
                    username = model.UserName;
                }

                bool authenticated = false;

                if (username.Contains("\\"))
                {
                    // Username contains domain
                    if (Sitecore.Security.Authentication.AuthenticationManager.Login(username, model.Password, model.RememberMe))
                    {
                        authenticated = true;
                    }
                }

                if (!authenticated)
                {
                    // login in CRM user without password
                    if (username.StartsWith("crm"))
                    {
                        if (Sitecore.Security.Authentication.AuthenticationManager.Login(username))
                        {
                            authenticated = true;
                        }
                    }
                }

                // try the default domain
                if (!authenticated)
                {
                    Sitecore.Security.Domains.Domain domain = Sitecore.Context.Domain;
                    string domainUser = domain.Name + @"\" + username;
                    if (Sitecore.Security.Authentication.AuthenticationManager.Login(domainUser, model.Password, model.RememberMe))
                    {
                        authenticated = true;
                    }
                }
           


                if (authenticated)
                { 
                    // Register Goal & set a few values in the visit tags.
                    AnalyticsHelper.RegisterGoalOnCurrentPage("Login", "[Login] Username: \"" + model.UserName + "\"");
                    AnalyticsHelper.SetVisitTagsOnLogin(model.UserName, false);
                    return RedirectToLocal("/my-account/");
                }
                
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "The user name or password provided is incorrect.");
            return View(model);
            
        }

        public ActionResult LogOff()
        {
            //throw new Exception("Hello there");
            Sitecore.Security.Authentication.AuthenticationManager.Logout();

            var tracker = Sitecore.Analytics.Tracker.Current;
            var manager = Sitecore.Configuration.Factory.CreateObject("tracking/contactManager", true) as Sitecore.Analytics.Tracking.ContactManager;
            ///TODO: need to update below for v9
            //manager.FlushContactToXdb(tracker.Contact);
            //manager.SaveAndReleaseContact(tracker.Contact);
            var ctxManager = Sitecore.Configuration.Factory.CreateObject("tracking/sessionContextManager", true) as Sitecore.Analytics.Data.SessionContextManagerBase;
            ctxManager.Submit(tracker.Session);
            Session.Abandon();
            Sitecore.Web.WebUtil.Redirect("/");
            return null;

        }

        public ActionResult ViewProfile()
        {
            var model = new ViewProfileModel();

            model.VisitDetails = new VisitInformation();

            if (Tracker.Current == null)
            {
                return View(model);
            }
            //var identifier = Tracker.Current.Contact.Identifiers.Identifier;

            //IKloutScoreUtils kloutScoreUtil = ExecutingContext.Current.IoC.TryGet<IKloutScoreUtils>(new IParameter[0]);
            //IKloutProfileManager kloutProfileManager = ExecutingContext.Current.IoC.TryGet<IKloutProfileManager>(new IParameter[0]);

            //if (kloutScoreUtil == null || kloutProfileManager == null)
            //{

            //}
            //else
            //{
            //    KloutProfile kloutProfile = kloutProfileManager.GetKloutProfile(Tracker.Current.Contact.ContactId.GetIdentifier());
            //    if (!kloutProfile.IsEmpty && !kloutProfile.KloutScore.IsEmpty)
            //    {
            //        model.KloutScore = kloutScoreUtil.RoundKloutScore(kloutProfile.KloutScore).ToString();
            //        if (kloutProfile.KloutScore.MonthChangeDelta > 0)
            //        {
            //            model.KloutTrend = 1;
            //        }
            //        else
            //        {
            //            model.KloutTrend = 0;
            //        }
            //    }
            //}

            return View(model);

        }


        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (model.Password != model.ConfirmPassword) throw new ApplicationException(GetDictionaryText("Passwords do not match"));

                    string domainUser = Sitecore.Context.Domain.GetFullName(model.Email);
                    if (Sitecore.Security.Accounts.User.Exists(domainUser)) throw new ApplicationException(GetDictionaryText("Already registered"));

                    System.Web.Security.Membership.CreateUser(domainUser, model.Password, model.Email);

                    if (Sitecore.Security.Authentication.AuthenticationManager.Login(domainUser, model.Password, false))
                    {
                        // Register Goal & set a few values in the visit tags.
                        Sitecore.Context.User.Profile.FullName = model.FirstName + " " + model.Surname;
                        Sitecore.Context.User.Profile.ProfileItemId = "{93B42F5F-17A9-441B-AB6D-444F714EF384}"; //LS User
                        Sitecore.Context.User.Profile.Save();

                        AnalyticsHelper.RegisterGoalOnCurrentPage("Register", "[Register] Username: \"" + domainUser + "\"");
                        AnalyticsHelper.SetVisitTagsOnLogin(domainUser, true);
                        Sitecore.Web.WebUtil.Redirect("/");
                    }
                }
                catch (System.Web.Security.MembershipCreateUserException)
                {
                    ModelState.AddModelError("", GetDictionaryText("Unable to register"));
                }
                catch (ApplicationException ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }


        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return Redirect("/");
            }
        }
    }
}