﻿using System.Collections.Generic;
using System.Web.Mvc;
using Officecore.Dev.Configuration;
using Officecore.Dev.Configuration.SiteUI;
using Officecore.Dev.Configuration.SiteUI.Base;
using Officecore.Dev.Models;
using Sitecore.Data.Items;

namespace Officecore.Dev.Controllers
{
    public class NavigationController : OfficecoreBaseController
    {
        public ActionResult Breadcrumbs()
        {
            if (Sitecore.Context.Item.ID != SiteConfiguration.GetHomeItem().ID)
            {
                List<SimpleItem> items = new List<SimpleItem>();
                Item temp = Sitecore.Context.Item;

                while (temp.ID != SiteConfiguration.GetHomeItem().ParentID)
                {
                    items.Add(new SimpleItem(temp));
                    temp = temp.Parent;
                }

                items.Reverse();
                return View(items);
            }
            return null;
        }

        public ActionResult LeftMenu()
        {
            Item home = SiteConfiguration.GetHomeItem();
            Item dataSource = DataSourceItemOrCurrentItem;

            var children = bool.Parse(UIHelpers.GetPropertyValue("showchildren", "false"));

            if (! children)
            {
                if (home.ID != dataSource.ID)  // if on the home node, just use it
                {
                    //   while (dataSource.ParentID != home.ID)
                    dataSource = dataSource.Parent;
                }
            }
            

            MenuItem ds = new MenuItem(dataSource);
            return (dataSource != null && ds.HasChildrenToShowInSecondaryMenu) ? View(ds) : ShowListIsEmptyPageEditorAlert();

        }

        private ActionResult ShowListIsEmptyPageEditorAlert()
        {
            return IsPageEditorEditing ? View("ShowPageEditorAlert", new PageEditorAlert(PageEditorAlert.Alerts.ListIsEmpty)) : null;
        }
    }
}