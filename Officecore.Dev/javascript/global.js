/** 
* Helper method for declaring namespaces 
*/
window.nspace = function (nspace, alias) {
    var parts = nspace.split(".");
    var obj = window;
    for (var i = 0; i < parts.length; i++) {
        obj[parts[i]] = obj[parts[i]] || {};
        obj = obj[parts[i]];
    }
};

/**
* Build a jQuery plugin interface to an object
* Will call initialize() method if available, passing
* in an options object with the element as the first arg
*/
window.pluginize = function (name, obj, constructor_args) {
    jQuery.fn[name] = function (args) {
        return this.each(function () {
            var element = jQuery(this);

            // args are passed using apply();
            // make element the first arg
            args = [args];
            args.splice(0, 0, element);

            if (element.data(name)) return;

            var plugin = new obj(constructor_args);
            if (jQuery.isFunction(plugin.initialize)) {
                plugin.initialize.apply(plugin, args);
            }
            element.data(name, plugin);
        });
    }
}
// open new window
function openWindow(url)
{
    var w = window.open(url);
    w.focus();
}


