﻿nspace("jetstream.system");

(function ($, _) {
    
    
    jetstream.system.PatchLink = function () {
        this.$element = null;
    };
    
    (function (prototype) {
        
        prototype.initialize = function (element, options) {
           
            this.$element = element;
            
            var self = this;
            
            if (self.$element.length && options.property.length && options.value.length) {
                $('a[href]').attr('href', function () {
                        linkHref = $(this).attr('href');
                        if (linkHref != '') {
                            if ((linkHref.indexOf(options.property) != -1) || (linkHref.indexOf('javascript:') != -1) || (linkHref.indexOf('PostBack') != -1) || (linkHref.indexOf('#') != -1))
                                return linkHref;
                            return linkHref + (linkHref.indexOf('?') != -1 ? '&' + options.property + '=' + options.value : '?' + options.property + '=' + options.value);
                        }
                        else return '#';
                });
                
            }
        };

    })(jetstream.system.PatchLink.prototype);
    
    window.pluginize("PatchLink", jetstream.system.PatchLink);

})(jQuery);