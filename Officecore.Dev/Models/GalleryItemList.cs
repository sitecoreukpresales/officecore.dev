﻿using Officecore.Dev.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Analytics;


namespace Officecore.Dev.Models
{
    public class GalleryItemList : CustomItem
    {
        public GalleryItemList(Item item) : base(item)
        {
            Assert.IsNotNull(item, "item");
        }

        public ID TagParent
        {
            get;set;
        }

        public IEnumerable<string> Tags
        {
            get
            {
                var taglist = Sitecore.Context.Database.GetItem(this.TagParent).Children.Where(x => x.TemplateName == "Tag");
                return taglist.Select(x => x.Name);
            }
        }

        public IEnumerable<SimpleItem> ChildItems
        {
            get
            {
                IEnumerable<SimpleItem> items = new SimpleItem(InnerItem).ChildrenInCurrentLanguage;
                return items;
            }
        }
    }
}