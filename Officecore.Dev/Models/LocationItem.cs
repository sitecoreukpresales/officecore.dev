﻿using Officecore.Dev.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Officecore.Dev.Models
{
    public class LocationItem : CustomItem
    {
        public LocationItem(Item item) : base(item)
        {
            Assert.IsNotNull(item, "item");
        }

        public string LocationName
        {
            get { return InnerItem[FieldId.LocationName]; }
        }

        public string Line1
        {
            get { return InnerItem[FieldId.Line1]; }
        }

        public string Line2
        {
            get { return InnerItem[FieldId.Line2]; }
        }

        public string City
        {
            get { return InnerItem[FieldId.City]; }
        }

        public string Postcode
        {
            get { return InnerItem[FieldId.Postcode]; }
        }

        public string Country
        {
            get { return InnerItem[FieldId.Country]; }
        }

        public string Phone
        {
            get { return InnerItem[FieldId.Phone]; }
        }

        public string Email
        {
            get { return InnerItem[FieldId.Email]; }
        }

        public string Lat
        {
            get { return InnerItem[FieldId.Lat]; }
        }

        public string Long
        {
            get { return InnerItem[FieldId.Long]; }
        }

        public Item Item
        {
            get { return InnerItem; }
        }


        public string Url
        {
            get { return LinkManager.GetItemUrl(InnerItem); }
        }

        public IEnumerable<LocationItem> ChildrenInCurrentLanguage
        {
            get
            {
                return InnerItem.Children.Select(x => new LocationItem(x)).Where(x => SiteConfiguration.DoesItemExistInCurrentLanguage(x.Item));
            }
        }

        public IEnumerable<LocationItem> Children
        {
            get
            {
                return InnerItem.Children.Select(x => new LocationItem(x));
            }
        }


        public static class FieldId
        {
            public static readonly ID LocationName = new ID("{912D6B17-1460-41E5-9FD9-0B4BE18C6CB2}");
            public static readonly ID Line1 = new ID("{F0A96261-4B0D-47EA-A134-BD24551A5979}");
            public static readonly ID Line2 = new ID("{8A4BF413-F5C6-4B33-A4DD-D644686593F8}");
            public static readonly ID City = new ID("{5A253626-E012-44B4-B40A-40EE48906598}");
            public static readonly ID Postcode = new ID("{51550A99-04F8-444F-A7EB-4205AD536F8C}");
            public static readonly ID Country = new ID("{42B437DB-86CB-4F74-BBF1-2D551F5F2EE4}");
            public static readonly ID Phone = new ID("{8D491B45-49D0-42C3-B373-50A7A4938C0F}");
            public static readonly ID Email = new ID("{703074C6-9796-43A9-8CD3-56260105BC28}");
            public static readonly ID Lat = new ID("{EB6ACAE0-5842-472F-9478-D5CB60DD3A62}");
            public static readonly ID Long = new ID("{C7C6866A-9555-47D5-8B58-26E3EA9CF197}");
        }
    }
}