﻿using Officecore.Dev.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Analytics;
using Officecore.Dev.Configuration.SiteUI;

namespace Officecore.Dev.Models
{
    public class CarouselItem : CustomItem
    {
        public CarouselItem(Item item) : base(item)
        {
            Assert.IsNotNull(item, "item");
        }

        public string Text
        {
            get { return InnerItem[FieldId.Text]; }
        }

        public string BackgroundImage
        {
            get { return InnerItem[FieldId.BackgroundImage]; }
        }

        public string ForegroundImage
        {
            get { return InnerItem[FieldId.ForegroundImage]; }
        }

        //public string TextColour
        //{
        //    get { return InnerItem[FieldId.TextColour]; }
        //}

        public string LinkItemUrl
        {
            get { return InnerItem.GetLink("Link Item"); }
        }

        //public string LinkText
        //{
        //    get
        //    {
        //        return InnerItem[FieldId.LinkText];
        //    }
        //}

        //public string Position
        //{
        //    get
        //    {
        //        var position = InnerItem[FieldId.Postition];
        //        switch( position)
        //        {
        //            case "Right":
        //                {
        //                    return "bannertextContainerRight";
        //                }
        //            case "Left":
        //                {
        //                    return "bannertextContainerLeft";
        //                }
        //            case "Top":
        //                {
        //                    return "bannertextContainerTop";
        //                }
        //            case "Bottom":
        //                {
        //                    return "bannertextContainerBottom";
        //                }
        //        }

        //        return InnerItem[FieldId.Postition];
        //    }
        //}

        //public bool ShowTextBox
        //{
        //    get { return InnerItem[FieldId.TextboxBackground]=="1" ? true : false; }
        //}

        public string GetTagValue(string TagName)
        {
            if (Tracker.Current == null)
            {
                return "";
            }

            if (string.IsNullOrEmpty(Tracker.Current.Contact.Tags[TagName]))
            {
                return "";
            }
            else
            {
                return Tracker.Current.Contact.Tags[TagName];
            }
        }

        public IEnumerable<CarouselItem> ChildrenInCurrentLanguage
        {
            get
            {
                return InnerItem.Children.Select(x => new CarouselItem(x)).Where(x => SiteConfiguration.DoesItemExistInCurrentLanguage(x.Item));
            }
        }

        public Item Item
        {
            get { return InnerItem; }
        }

        public IEnumerable<CarouselItem> Children
        {
            get
            {
                return InnerItem.Children.Select(x => new CarouselItem(x));
            }
        }

        public static class FieldId
        {
            public static readonly ID BackgroundImage = new ID("{3EED451B-D15C-44A1-997F-377ADA28C11E}");
            public static readonly ID ForegroundImage = new ID("{2B21752E-E0CF-45B5-BC89-C04F184E5B11}");
            public static readonly ID Text = new ID("{76F3F31B-7F3E-4B92-8B56-D9B091A6F632}");
            //public static readonly ID Title = new ID("{38049D1F-C43C-47C9-B527-C9A11029408C}");
            //public static readonly ID TextColour = new ID("{0370F592-F08A-4007-9B28-9D3EA7C2622C}");
            //public static readonly ID Postition = new ID("{CDB8194D-C18E-4769-83ED-097AAAE23208}");
            //public static readonly ID LinkText = new ID("{5B391C81-53EC-4808-B040-76DB67E3DE26}");
            //public static readonly ID TextboxBackground = new ID("{BEF0C653-A83C-4F93-9D33-060DA78FC775}");
        }
    }
}