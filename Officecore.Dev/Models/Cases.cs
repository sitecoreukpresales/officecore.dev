﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Officecore.Dev.Models
{
    public class Cases
    {
        public string Title
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public string Status
        {
            get;
            set;
        }

        public string Id
        {
            get;
            set;
        }

        public DateTime DateCreated
        {
            get;
            set;
        }

        public string DateUpdated
        {
            get;
            set;
        }
    }
}
