﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Officecore.Dev.Models
{

    public class UsersContext : DbContext
    {
        public UsersContext() : base("DefaultConnection") { }

        public DbSet<UserProfile> UserProfiles { get; set; }
    }

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }
    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Surname")]
        public string Surname { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "The email address is not valid")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [DataType(DataType.Custom)]
        [Display(Name = "Signup for Newsletter")]
        public bool NewsLetter { get; set; }
    }

    public class UpdateProfileModel
    {

        [Required]
        [Display(Name = "Name")]
        public string FullName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "The email address is not valid")]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "Interests")]
        public string Interests { get; set; }

        [Display(Name = "Interests")]
        public System.Web.Mvc.SelectList InterestTypes
        {
            get;
            set;
        }

    }

    public class ViewProfileModel
    {

        public ViewProfileModel()
        {
            Firstname = Sitecore.Context.User.Profile["Firstname"];
            Surname = Sitecore.Context.User.Profile["Surname"];
            Email  = Sitecore.Context.User.Profile.Email;
            Phone = Sitecore.Context.User.Profile["Phone"];
            Company = Sitecore.Context.User.Profile["Company"];
            Jobtitle = Sitecore.Context.User.Profile["JobTitle"];
            Address1 = Sitecore.Context.User.Profile["Address1"];
            Address2 = Sitecore.Context.User.Profile["Address2"];
            Address3 = Sitecore.Context.User.Profile["Address3"];
            City = Sitecore.Context.User.Profile["City"];
            Country = Sitecore.Context.User.Profile["Country"];
            CallBackMessage = Sitecore.Context.User.Profile["CallbackMessage"];
            CallBackRecieved = Sitecore.Context.User.Profile["Callback"];
            DoNotEmail = Sitecore.Context.User.Profile["DoNotEmail"];
        }


        public string Firstname { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Company { get; set; }
        public string Jobtitle { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string TaggedLocation { get; set; }
        public string KloutScore { get; set; }
        public int KloutTrend { get; set; }
        public string CallBackRecieved { get; set; }
        public string CallBackMessage { get; set; }

        public string DoNotEmail { get; set; }

        public VisitInformation VisitDetails { get; set; }
    }

    public class DetachModel
    {
        public string UserIdentity { get; set; }
        public string Network { get; set; }
    }

}
