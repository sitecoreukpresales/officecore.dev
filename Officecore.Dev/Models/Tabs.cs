﻿using Officecore.Dev.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Analytics;
using Officecore.Dev.Configuration.SiteUI;
using static Officecore.Dev.Configuration.SiteConfiguration;
using Sitecore.Links;

namespace Officecore.Dev.Models
{
    public class Tabs : CustomItem
    {
        public Tabs(Item item) : base(item)
        {
            Assert.IsNotNull(item, "item");
        }

        public Item Item
        {
            get { return InnerItem; }
        }

        public bool HasTabs
        {
            get
            {
                if (InnerItem.Children.Where(x => x.TemplateID == FieldId.TabFolderTemplateID).Count() > 0)
                {
                    return true;
                }
                return false;
            }
        }

        public IEnumerable<Tab> ChildTabs
        {
            get
            {
               // List<Tab> childTabs = new List<Tab>();
                Item tab = InnerItem.Children.Where(x => x.TemplateID == FieldId.TabFolderTemplateID).FirstOrDefault();
                IEnumerable<Tab> childTabs = tab.Children.Where(t => t.TemplateID == FieldId.TabTemplateID).Select(x => new Tab(x));
                return childTabs;
            }
        }

        public static class FieldId
        {
            public static readonly ID TabFolderTemplateID = new ID("{7B39F0D2-512E-4DC2-A478-E22E95FDBD9C}");
            public static readonly ID TabTemplateID = new ID("{FAEDCC41-E96F-4565-84B9-397259A3B3DE}");
        }

    }


    public class Tab
    {
        Item InnerItem;
        public Tab(Item TabItem) { InnerItem = TabItem; }

        public string TabName
        {
            get
            {
                return InnerItem[FieldId.Name];
            }
        }

        public string Title
        {
            get
            {
                return InnerItem[FieldId.Title];
            }
        }

        public string Text
        {
            get
            {
                return InnerItem[FieldId.Text];
            }
        }

        public string Image
        {
            get
            {
                return InnerItem[FieldId.Image];
            }
        }

        public string Url
        {
            get { return LinkManager.GetItemUrl(InnerItem); }
        }

        public Item TabItem
        {
            get
            {
                return InnerItem;
            }
        }

        public static class FieldId
        {
            public static readonly ID Name = new ID("{D547796D-9183-45A7-9A76-70445BFBB88F}");
            public static readonly ID Image = new ID("{33BD8C22-AD75-4C83-BB2E-2299F823F719}");
            public static readonly ID Title = new ID("{B070C3CF-C82E-4A1B-8DB8-9FF99E929A0D}");
            public static readonly ID Text = new ID("{7535D9B2-B818-4653-A65A-B7102CCD3804}");
        }
    }
}