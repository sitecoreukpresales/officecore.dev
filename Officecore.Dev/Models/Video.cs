﻿using Officecore.Dev.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Analytics;
using Officecore.Dev.Configuration.SiteUI;
using static Officecore.Dev.Configuration.SiteConfiguration;
using Sitecore.Data.Fields;

namespace Officecore.Dev.Models
{
    public class Video : CustomItem
    {
        public Video(Item item) : base(item)
        {
            Assert.IsNotNull(item, "item");
        }

        public Item[] Videos
        {
            get
            {
                var VideoField = (MultilistField)InnerItem.Fields["Video"];
                if (VideoField != null)
                {
                    return VideoField.GetItems();
                }

                return null;
            }
        }
    }
}
