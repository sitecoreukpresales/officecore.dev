﻿using Officecore.Dev.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Officecore.Dev.Models
{
    public class NewsItem : CustomItem
    {
        public NewsItem(Item item) : base(item)
        {
            Assert.IsNotNull(item, "item");
        }

        public string Title
        {
            get { return InnerItem[FieldId.Title]; }
        }

        public string Text
        {
            get { return InnerItem[FieldId.Text]; }
        }


        public DateTime Date
        {
            get { return ((DateField) InnerItem.Fields[FieldId.Date]).DateTime; }
        }

        public string Summary
        {
            get { return InnerItem[FieldId.Summary]; }
        }

        public string TeaserImage
        {
            get { return InnerItem[FieldId.TeaserImage]; }
        }

        public string Image
        {
            get { return InnerItem[FieldId.Image]; }
        }

        public Item Item
        {
            get { return InnerItem; }
        }

        public string Url
        {
            get { return LinkManager.GetItemUrl(InnerItem); }
        }

        public IEnumerable<NewsItem> ChildrenInCurrentLanguage
        {
            get
            {
                return InnerItem.Children.Select(x => new NewsItem(x)).Where(x => SiteConfiguration.DoesItemExistInCurrentLanguage(x.Item));
            }
        }

        public static class FieldId
        {
            public static readonly ID Date = new ID("{234542DC-C610-4CA8-BAA6-2592A8BCB1D7}");
            public static readonly ID Summary = new ID("{D7229DBA-B952-4D82-A5A0-459C69618D45}");
            public static readonly ID TeaserImage = new ID("{00E1D306-96BD-4B32-85B4-CD63C53CC6C1}");
            public static readonly ID Title = new ID("{5A5684BB-8B54-44F6-ABCC-2BADA05ADA5D}");
            public static readonly ID Text = new ID("{2B60D8C1-81DB-45A7-B1CB-654CDDA96AE3}");
            public static readonly ID Image = new ID("{2B60D8C1-81DB-45A7-B1CB-654CDDA96AE3}");
            public static readonly ID Tags = new ID("{2B60D8C1-81DB-45A7-B1CB-654CDDA96AE3}");
        }
    }
}
