﻿using Officecore.Dev.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Analytics;
using Officecore.Dev.Configuration.SiteUI;
using static Officecore.Dev.Configuration.SiteConfiguration;

namespace Officecore.Dev.Models
{
    public class BlockQuote : CustomItem
    {
        public BlockQuote(Item item) : base(item)
        {
            Assert.IsNotNull(item, "item");
        }
        public string Quote
        {
            get { return InnerItem[FieldId.Quote]; }
        }

        public string Link
        {
            get { return InnerItem.GetLinkFieldURL(InnerItem.Fields[FieldId.Link]); }

        }

        public string LinkText
        {
            get { return InnerItem[FieldId.LinkText]; }
        }

        public string QuoteIcon
        {
            get { return InnerItem[FieldId.Icon]; }
        }

        public SiteTheme Colour
        {
            get { return new SiteTheme(Sitecore.Context.Database.GetItem(new ID(InnerItem.Fields[FieldId.Color].Value))); }
        }

        public SiteTheme HoverColour
        {
            get { return new SiteTheme(Sitecore.Context.Database.GetItem(new ID(InnerItem.Fields[FieldId.HoverColor].Value))); }
        }

        public Item Item
        {
            get { return InnerItem; }
        }


        public static class FieldId
        {
            public static readonly ID Quote = new ID("{880B22DD-E04F-4A7E-A3A3-DCA5AFB723EE}");
            public static readonly ID Link = new ID("{9407998A-8AC1-46AA-9CAB-005C715F3FD2}");
            public static readonly ID LinkText = new ID("{27A9C845-F70A-42F9-A5D5-6BCDB28060DF}");
            public static readonly ID Icon = new ID("{8F8ED436-DB8A-48F2-83FC-961EC66ED947}");
            public static readonly ID Color = new ID("{FC382C03-7BE6-4C9E-BEE3-BB7A5C83599A}");
            public static readonly ID HoverColor = new ID("{75EEC381-2D7D-41C4-A950-77564469A91E}");
        }
    }
}