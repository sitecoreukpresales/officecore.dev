﻿using Officecore.Dev.Configuration;
using Officecore.Dev.Configuration.SiteUI;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Officecore.Dev.Models
{
    public class Clients : CustomItem
    {
        public Clients(Item item) : base(item)
        {
            Assert.IsNotNull(item, "item");
        }

        public Item Item
        {
            get { return InnerItem; }
        }

        public string Url
        {
            get { return LinkManager.GetItemUrl(InnerItem); }
        }

        public string WebsiteLinnk
        {
            get { return InnerItem.GetLinkFieldURL(InnerItem.Fields[FieldId.WebsiteLink]); }
        }


        public DateTime Created
        {
            get
            {
                DateField dt = (DateField)InnerItem.Fields[Sitecore.FieldIDs.Created];
                return dt.DateTime;
            }
        }

        /// <summary>
        /// Return the first Tag Only
        /// </summary>
        public string Tag
        {
            get
            {
                var tagField = (MultilistField)InnerItem.Fields[FieldId.Tags];
                if(tagField != null)
                {
                    Item[] items = tagField.GetItems();
                    return items.FirstOrDefault().Name;
                }
                return "";
            }
        }

        public IEnumerable<Clients> ChildrenInCurrentLanguage
        {
            get
            {
                return InnerItem.Children.Select(x => new Clients(x)).Where(x => SiteConfiguration.DoesItemExistInCurrentLanguage(x.Item));
            }
        }

        public static class FieldId
        {
            public static readonly ID WebsiteLink = new ID("{46FDB2EF-D6C6-46F7-A2B1-93B9910FEBB8}");
            public static readonly ID ImageOn = new ID("{165F0491-CDA3-408A-BA9A-E73740D65DAD}");
            public static readonly ID ImageOff = new ID("{50881F1E-7324-412C-9FBB-DE9AD64BCFF8}");
            public static readonly ID Tags = new ID("{17429EE7-6264-46AA-A7CA-6C1106417700}");

        }
    }
}
