﻿using Officecore.Dev.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Officecore.Dev.Configuration.SiteUI;
using Sitecore.Data.Fields;

namespace Officecore.Dev.Models
{
    public class ReturnRequest : CustomItem
    {
        public ReturnRequest(Item item) : base(item)
        {
            Assert.IsNotNull(item, "item");
        }


        public string Fullname
        {
            get { return InnerItem[FieldId.Fullname]; }
        }

        public string Email
        {
            get { return InnerItem[FieldId.Email]; }
        }

        public string Reason
        {
            get { return InnerItem[FieldId.Reason]; }
        }

        public string Product
        {
            get { return InnerItem[FieldId.Product]; }
        }

        public string Status
        {
            get
            {
                return InnerItem[FieldId.Status];
            }
        }

        public DateTime Created
        {
            get
            {
                DateField dt = (DateField)InnerItem.Fields[Sitecore.FieldIDs.Created];
                return dt.DateTime;
            }
        }

        public string Updated
        {
            get
            {
                DateField dt = (DateField)InnerItem.Fields[Sitecore.FieldIDs.Updated];
                return dt.DateTime.ToString("dd MMM yyyy HH:mm");
            }
        }

        public Item Item
        {
            get { return InnerItem; }
        }



        public static class FieldId
        {
            public static readonly ID Email = new ID("{4036F9A0-0F6A-4B11-AC3A-0D364FB035F3}");
            public static readonly ID Fullname = new ID("{7168F3B6-95EA-4EC3-969A-0D17A1B9FFC8}");
            public static readonly ID Status = new ID("{2B1D0B96-6952-410B-AD95-94D893AE0919}");
            public static readonly ID Reason = new ID("{EA383C2A-3196-48C3-8D82-64A054304D11}");
            public static readonly ID Product = new ID("{358B5ED5-FD5E-4543-810B-D9E7E9FE2706}");
        }
    }
}