﻿using Officecore.Dev.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Analytics;
using Sitecore.Data.Fields;
using Sitecore.Resources.Media;
using Sitecore.Mvc.Presentation;

namespace Officecore.Dev.Models
{
    public class MediaTabItems : IRenderingModel
    {
        Item item;
        
        public void Initialize(Sitecore.Mvc.Presentation.Rendering rendering)
        {
            item = rendering.Item;
        }

        public IEnumerable<string> ImageUrls
        {
            get
            {
                List<string> urls = new List<string>();
                var images = (MultilistField)item.Fields["images"];
                foreach (var item in images.GetItems())
                {
                    urls.Add(MediaManager.GetMediaUrl(item));
                }

                return urls;
            }
        }

        public IEnumerable<string> VideoUrls
        {
            get
            {
                List<string> urls = new List<string>();
                var videos= (MultilistField)item.Fields["video"];
                foreach (var item in videos.GetItems())
                {
                    urls.Add(MediaManager.GetMediaUrl(item));
                }

                return urls;
            }
        }

    }
}