﻿using Officecore.Dev.Configuration;
using Officecore.Dev.Configuration.SiteUI;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Officecore.Dev.Models
{
    public class Chart : CustomItem
    {
        public Chart(Item item) : base(item)
        {
            Assert.IsNotNull(item, "item");
        }

        public Item Item
        {
            get { return InnerItem; }
        }

        public string Title {
            get
            {
                return InnerItem[FieldId.Title];
            }
        }

        public string ChartType { get
            {
                return InnerItem[FieldId.ChartType];
            }
        }
        public string Width
        {
            get
            {
                return InnerItem[FieldId.Width];
            }
        }
        public string Height
        {
            get
            {
                return InnerItem[FieldId.Height];
            }
        }
        public string Background
        {
            get
            {
                return InnerItem[FieldId.Background];
            }
        }

        public IEnumerable<ChartData> Data
        {
            get
            {
                List<ChartData> data = new List<ChartData>();
                if (InnerItem.HasChildren)
                {
                    foreach (Item item in InnerItem.GetChildren())
                    {
                        data.Add(new ChartData() { Field = item["Field"] , Value = Int32.Parse(item["Value"]) });
                    } 
                }

                return data;
            }
        }


        public static class FieldId
        {
            public static readonly ID Title = new ID("{8B12007F-86C5-49A8-8C35-935E3A73E33D}");
            public static readonly ID ChartType = new ID("{740203F6-D3FC-4A02-9112-FC33D6050D48}");
            public static readonly ID Width = new ID("{CB4F46C3-9E13-4B96-A682-71DBC6DBA98C}");
            public static readonly ID Height = new ID("{D0757839-FEC8-42CF-8023-254DBB6B2623}");
            public static readonly ID Background = new ID("{06E0B3E8-CE6D-489C-9AE1-FA8D8420F296}");

        }

        public class ChartData
        {
            public string Field { get; set; }

            public int Value { get; set; }
        }
    
    }
}