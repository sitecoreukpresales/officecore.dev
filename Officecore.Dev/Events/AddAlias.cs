﻿using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Officecore.Dev.Events
{
    public class AddAlias
    {
        public string Database
        {
            get;
            set;
        }
        public void OnItemSaved(object sender, EventArgs args)
        {

            Assert.ArgumentNotNull(sender, "sender");
            Assert.ArgumentNotNull(args, "args");
            Item item = Event.ExtractParameter(args, 0) as Item;

            if (item.Database != null && String.Compare(item.Database.Name, this.Database) != 0)
            {
                return;
            }

            if (item == null)
            {
                return;
            }

            if (item.TemplateID != new Sitecore.Data.ID("{7139A8B6-68D9-471F-96E4-B87ED703C185}"))
            {
                return;
            }

            if (item.Database.GetItem("/sitecore/system/aliases/" + item.Name) == null)
            {

                Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");

                Sitecore.Data.Items.Item aliasesFolder = master.GetItem("/sitecore/system/aliases");
                Sitecore.Data.Items.TemplateItem aliasTemplate = master.Templates[Sitecore.TemplateIDs.Alias];



                Sitecore.Data.Items.Item itemAliase = aliasesFolder.Add(item.Name, aliasTemplate);

                itemAliase.Editing.BeginEdit();
                Sitecore.Data.Fields.LinkField linkField = itemAliase.Fields["linked item"];
                linkField.LinkType = "internal";
                Sitecore.Links.UrlOptions urlOptions =
                Sitecore.Links.LinkManager.GetDefaultUrlOptions();
                urlOptions.AlwaysIncludeServerUrl = false;
                linkField.Url = Sitecore.Links.LinkManager.GetItemUrl(item, urlOptions);
                linkField.TargetID = item.ID;
                itemAliase.Appearance.Icon = item.Appearance.Icon;
                itemAliase.Editing.EndEdit();

            }
        }
    }
}