﻿using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Data.Validators;
using Sitecore.Diagnostics;
using Sitecore.Shell.Applications.ContentEditor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Officecore.Dev.Configuration.Validators
{
    [Serializable]
    public class ImageValidator : StandardValidator
    {
        public ImageValidator() { }

        public ImageValidator(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
        public override string Name
        {
            get
            {
                return "Image Validator";
            }
        }

        protected override ValidatorResult Evaluate()
        {
            int minWidth = MainUtil.GetInt(Parameters["MinWidth"], 0);
            int minHeight = MainUtil.GetInt(Parameters["MinHeight"], 0);
            int maxHeight = MainUtil.GetInt(Parameters["MaxHeight"], 0);
            int maxWidth = MainUtil.GetInt(Parameters["MaxWidth"], 0);
            int minDpi = MainUtil.GetInt(Parameters["MinDpi"], 0);

            if (ItemUri != null)
            {
                var media = MediaItem;

                if (media == null)
                {
                    return ValidatorResult.Valid;
                }

                int width = MainUtil.GetInt(media.InnerItem["Width"], 0);
                int height = MainUtil.GetInt(media.InnerItem["Height"], 0);

                if (minWidth == maxWidth && minHeight == maxHeight && (width != minWidth || height != minHeight))
                    return GetResult("The image referenced in the Image field \"{0}\" does not match the size requirements. Image needs to be exactly {1}x{2} but is {3}x{4}",
                                      media.DisplayName, minWidth.ToString(), minHeight.ToString(), width.ToString(),
                                     height.ToString());

                if (width < minWidth)
                    return GetResult("The image referenced in the Image field \"{0}\" is too small. The width needs to be at least {1} pixels but is {2}.",
                                      media.DisplayName, minWidth.ToString(), width.ToString());

                if (height < minHeight)
                    return GetResult("The image referenced in the Image field \"{0}\" is too small. The height needs to be at least {1} pixels but is {2}.",
                                      media.DisplayName, minHeight.ToString(), height.ToString());

                if (width > maxWidth)
                    return GetResult("The image referenced in the Image field \"{0}\" is too big. The width needs to at most {1} pixels but is {2}.",
                                      media.DisplayName, maxWidth.ToString(), width.ToString());

                if (height > maxHeight)
                    return GetResult("The image referenced in the Image field \"{0}\" is too big. The height needs to be at most {1} pixels but is {2}.",
                                      media.DisplayName, maxHeight.ToString(), height.ToString());


                System.Drawing.Image i = System.Drawing.Image.FromStream(media.GetMediaStream());
                if (i.HorizontalResolution < minDpi)
                {
                    return GetResult("The image referenced in the Image field \"{0}\" resolution is to low, the resolution needs to be at least {1} dpi but is {2} dpi.", media.DisplayName, minDpi.ToString(), i.HorizontalResolution.ToString());
                }

            }

            return ValidatorResult.Valid;

        }

        protected override ValidatorResult GetMaxValidatorResult()
        {
            return GetFailedResult(ValidatorResult.CriticalError);
        }

        private ValidatorResult GetResult(string text, params string[] arguments)
        {
            Text = GetText(text, arguments);
            return GetFailedResult(ValidatorResult.Warning);
        }

        private MediaItem MediaItem
        {
            get
            {
                ItemUri itemUri = ItemUri;


                Database database = Factory.GetDatabase(itemUri.DatabaseName);
                Assert.IsNotNull(database, itemUri.DatabaseName);
                return database.GetItem(new ID(ItemUri.ItemID.Guid));

            }
        }
    }
}
