﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Analytics.Data;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Officecore.Dev.Configuration.Helpers.Extensions;

namespace Officecore.Dev.Configuration.SiteUI.Search.ComputedFields
{
    class ContactCampaignIdsField : AbstractComputedIndexField, ISearchIndexInitializable
    {
        private ISearchIndex index;

        public ContactCampaignIdsField()
        {
        }

        public ContactCampaignIdsField(System.Xml.XmlNode configNode)
            : this()
        {
        }

        public void Initialize(ISearchIndex searchIndex)
        {
            index = searchIndex;
        }

        public override object ComputeFieldValue(IIndexable indexable)
        {
            if (indexable.GetFieldByName("type") == null)
            {
                return null;
            }

            if (indexable.GetFieldByName("type").Value.ToString() != "contact")
            {
                // Only process contacts
                return null;
            }

            var contactId = new Guid(indexable.GetFieldByName("contact.ContactId").Value.ToString());

            var repository = new ContactRepository();

            var interactions = repository.LoadHistoricalInteractions(contactId, int.MaxValue, null, null);

            var campaignIds = interactions.
                Where(interaction => interaction.CampaignId.HasValue).
                Select(interaction => interaction.CampaignId.Value.ToString().Replace("-", string.Empty)).
                Distinct().
                ToArray();

            return campaignIds.Length == 0 ? "_NONE_" : campaignIds.Join(" ");
        }


    }
}
