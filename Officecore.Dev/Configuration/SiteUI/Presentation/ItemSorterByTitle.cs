﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore.Data.Items;
using Sitecore.Data.Comparers;

namespace Officecore.Dev.Configuration.SiteUI.Presentation
{
    public class ItemSorterByTitle : Comparer
    {
        protected override int DoCompare(Item item1, Item item2)
        {
            string x = item1["title"];
            string y = item2["title"];

            return x.CompareTo(y);
        }
    }
}
