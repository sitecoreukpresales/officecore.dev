﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Officecore.Dev.Configuration.Helpers
{
    public class JsonHelpers
    {
        public static string GetValueFromJson(string json)
        {
            dynamic items = JsonConvert.DeserializeObject(json);

            string itemstring = string.Empty;
            int i = 0;
            foreach (dynamic item in items)
            {
                if (i % 2 == 0)
                {
                    itemstring += item.name.Value + ", ";
                }
                else
                {
                    itemstring += "<strong>" + item.name.Value + "</strong>, ";
                }
                i++;
            }

            return itemstring.Remove(itemstring.Length - 2);
        }

        public static string GetWorkHistory(string json)
        {
            dynamic items = JsonConvert.DeserializeObject(json);

            string itemstring = string.Empty;

            foreach (dynamic item in items)
            {
                itemstring += item.employer.name.Value + ", ";
            }
            return itemstring.Remove(itemstring.Length - 2);
        }

        public static string GetTaggedLocations(string json)
        {
            dynamic items = JsonConvert.DeserializeObject(json);

            string itemstring = string.Empty;
            int i = 0;

            foreach (dynamic item in items)
            {
                if (i % 2 == 0)
                {
                    try
                    {
                        itemstring += item.place.name.Value + " - " + item.place.location.city.Value + ", " + item.place.location.country.Value.Replace("United Kingdom", "UK") + ", ";
                    }
                    catch { }
                }
                else
                {
                    try
                    {
                        itemstring += "<strong>" + item.place.name.Value + " - " + item.place.location.city.Value + ", " + item.place.location.country.Value.Replace("United Kingdom", "UK") + "</strong>, ";
                    }
                    catch { }
                }
                i++;
            }
            return itemstring.Remove(itemstring.Length - 2);
        }

        public static string GetLocation(string json)
        {
            dynamic items = JsonConvert.DeserializeObject(json);
            if (items != null)
            {
                return items.name.Value;
            }
            return "";
        }
    }
}