﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Officecore.Dev.Configuration.Helpers.Extensions
{
    public static class IEnumerableExtensions
    {
        public static string Join<T>(this IEnumerable<T> target, string separator)
        {
            return target == null
                ? string.Empty
                : string.Join(separator, target.Where(value => value != null).Select(value => value.ToString()).ToArray());
        }
    }
}
