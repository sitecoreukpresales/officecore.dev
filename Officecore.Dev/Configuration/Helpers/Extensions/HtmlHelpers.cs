﻿using Sitecore.Collections;
using Sitecore.Data.Items;
using Sitecore.Mvc.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Officecore.Dev.Configuration.Helpers.Extensions
{
    public static class HtmlHelpers
    {
        public static HtmlString RenderField(this Sitecore.Mvc.Helpers.SitecoreHelper sitecoreHelper, string fieldNameOrId, Item item, bool disableWebEdit = false, SafeDictionary<string> parameters = null)
        {
            if (parameters == null)
            {
                parameters = new SafeDictionary<string>();
            }

            return sitecoreHelper.Field(fieldNameOrId, item,
                new
                {
                    DisableWebEdit = disableWebEdit,
                    Parameters = parameters
                });    
        }



        public static HtmlString RenderDate(this Sitecore.Mvc.Helpers.SitecoreHelper sitecoreHelper, string fieldNameOrId, Item item, string format = "D", bool disableWebEdit = false,  bool setCulture = true, SafeDictionary<string> parameters = null)
        {
            if (setCulture)
            {
                Thread.CurrentThread.CurrentUICulture =
                  new CultureInfo(Sitecore.Context.Language.Name);
                Thread.CurrentThread.CurrentCulture =
                  CultureInfo.CreateSpecificCulture(Sitecore.Context.Language.Name);
            }

            if (parameters == null)
            {
                parameters = new Sitecore.Collections.SafeDictionary<string>();
            }

            parameters["format"] = format;
            return RenderField(
              sitecoreHelper,
              fieldNameOrId,
              item,
              disableWebEdit,
              parameters);
        }

    }
}


