﻿using Sitecore.Pipelines.Save;

namespace Officecore.Dev.Configuration.AuthoringExperience.Processors
{
    public class RemoveNonBreakingSpacesSaveProcessor
    {
        public void Process(SaveArgs args)
        {
            foreach (Sitecore.Pipelines.Save.SaveArgs.SaveItem saveItem in args.Items)
            {
                foreach (Sitecore.Pipelines.Save.SaveArgs.SaveField saveField in saveItem.Fields)
                {
                    // remove the &nbsp; characters
                    try
                    {
                        saveField.Value = saveField.Value.Replace("&nbsp;", " ");
                    }
                    catch
                    {

                    }
                }
            }
        }
    }
}