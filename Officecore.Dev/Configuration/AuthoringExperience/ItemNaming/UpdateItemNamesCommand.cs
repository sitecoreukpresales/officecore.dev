﻿using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.SecurityModel;
using Sitecore.Shell.Framework.Commands;

namespace Officecore.Dev.Configuration.AuthoringExperience.ItemNaming
{
    public class UpdateItemNamesCommand : Command
    {
        public override void Execute(CommandContext context)
        {
            //Disable security and create accounts
            using (new SecurityDisabler())
            {
                Database master = Factory.GetDatabase("master");
                Item content = master.GetItem("/sitecore/content");
                ItemNamingHelper.RecursiveItemSave(content, true);
            }
        }
    }
}
