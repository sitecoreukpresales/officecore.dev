﻿using System;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Events;

namespace Officecore.Dev.Configuration.AuthoringExperience.ItemNaming
{
    public class ItemNameHyphenHandler
    {

        protected void OnItemSaving(object sender, EventArgs args)
        {
            //ensures arguments aren't null
            Assert.ArgumentNotNull(sender, "sender");
            Assert.ArgumentNotNull(args, "args");

            //gets item parameter from event arguments
            object obj = Event.ExtractParameter(args, 0);
            Item item = obj as Item;
            if (item != null)
            {
                ItemNamingHelper.UpdateItemNames(item);
            }
        }
        
    }
}