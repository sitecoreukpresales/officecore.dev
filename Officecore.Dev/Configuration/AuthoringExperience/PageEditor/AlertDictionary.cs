﻿using System.Collections.Generic;
using Officecore.Dev.Configuration;
using Sitecore.Data.Items;

namespace Officecore.Dev.Configuration.AuthoringExperience.PageEditor
{
    public class AlertDictionary : Dictionary<string, string>
    {
        public AlertDictionary()
        {
            foreach (Item item in SiteConfiguration.GetPageEditorAlertsRootItem().Children)
            {
                Add(item["Key"], item["Alert"]);
            }
        }
    }
}