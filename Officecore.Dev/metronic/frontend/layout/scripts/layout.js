var Layout = function () {

     // IE mode
    var isRTL = false;
    var isIE8 = false;
    var isIE9 = false;
    var isIE10 = false;
    var isIE11 = false;

    var responsive = true;

    var responsiveHandlers = [];

    var handleInit = function() {

        if (jQuery2('body').css('direction') === 'rtl') {
            isRTL = true;
        }

        isIE8 = !! navigator.userAgent.match(/MSIE 8.0/);
        isIE9 = !! navigator.userAgent.match(/MSIE 9.0/);
        isIE10 = !! navigator.userAgent.match(/MSIE 10.0/);
        isIE11 = !! navigator.userAgent.match(/MSIE 11.0/);
        
        if (isIE10) {
            jQuery2('html').addClass('ie10'); // detect IE10 version
        }
        if (isIE11) {
            jQuery2('html').addClass('ie11'); // detect IE11 version
        }
    }

    // runs callback functions set by App.addResponsiveHandler().
    var runResponsiveHandlers = function () {
        // reinitialize other subscribed elements
        for (var i in responsiveHandlers) {
            var each = responsiveHandlers[i];
            each.call();
        }
    }

    // handle the layout reinitialization on window resize
    var handleResponsiveOnResize = function () {
        var resize;
        if (isIE8) {
            var currheight;
            jQuery2(window).resize(function () {
                if (currheight == document.documentElement.clientHeight) {
                    return; //quite event since only body resized not window.
                }
                if (resize) {
                    clearTimeout(resize);
                }
                resize = setTimeout(function () {
                    runResponsiveHandlers();
                }, 50); // wait 50ms until window resize finishes.                
                currheight = document.documentElement.clientHeight; // store last body client height
            });
        } else {
            jQuery2(window).resize(function () {
                if (resize) {
                    clearTimeout(resize);
                }
                resize = setTimeout(function () {
                    runResponsiveHandlers();
                }, 50); // wait 50ms until window resize finishes.
            });
        }
    }

    var handleIEFixes = function() {
        //fix html5 placeholder attribute for ie7 & ie8
        if (isIE8 || isIE9) { // ie8 & ie9
            // this is html5 placeholder fix for inputs, inputs with placeholder-no-fix class will be skipped(e.g: we need this for password fields)
            jQuery2('input[placeholder]:not(.placeholder-no-fix), textarea[placeholder]:not(.placeholder-no-fix)').each(function () {

                var input = jQuery2(this);

                if (input.val() == '' && input.attr("placeholder") != '') {
                    input.addClass("placeholder").val(input.attr('placeholder'));
                }

                input.focus(function () {
                    if (input.val() == input.attr('placeholder')) {
                        input.val('');
                    }
                });

                input.blur(function () {
                    if (input.val() == '' || input.val() == input.attr('placeholder')) {
                        input.val(input.attr('placeholder'));
                    }
                });
            });
        }
    }

    // Handles scrollable contents using jQuery SlimScroll plugin.
    var handleScrollers = function () {
        jQuery2('.scroller').each(function () {
            var height;
            if (jQuery2(this).attr("data-height")) {
                height = jQuery2(this).attr("data-height");
            } else {
                height = jQuery2(this).css('height');
            }
            jQuery2(this).slimScroll({
                allowPageScroll: true, // allow page scroll when the element scroll is ended
                size: '7px',
                color: (jQuery2(this).attr("data-handle-color")  ? jQuery2(this).attr("data-handle-color") : '#bbb'),
                railColor: (jQuery2(this).attr("data-rail-color")  ? jQuery2(this).attr("data-rail-color") : '#eaeaea'),
                position: isRTL ? 'left' : 'right',
                height: height,
                alwaysVisible: (jQuery2(this).attr("data-always-visible") == "1" ? true : false),
                railVisible: (jQuery2(this).attr("data-rail-visible") == "1" ? true : false),
                disableFadeOut: true
            });
        });
    }

    var handleSearch = function() {    
        jQuery2('.search-btn').click(function () {            
            if(jQuery2('.search-btn').hasClass('show-search-icon')){
                if (jQuery2(window).width()>767) {
                    jQuery2('.search-box').fadeOut(300);
                } else {
                    jQuery2('.search-box').fadeOut(0);
                }
                jQuery2('.search-btn').removeClass('show-search-icon');
            } else {
                if (jQuery2(window).width()>767) {
                    jQuery2('.search-box').fadeIn(300);
                } else {
                    jQuery2('.search-box').fadeIn(0);
                }
                jQuery2('.search-btn').addClass('show-search-icon');
            } 
        }); 

        // close search box on body click
        if(jQuery2('.search-btn').size() != 0) {
            jQuery2('.search-box, .search-btn').on('click', function(e){
                e.stopPropagation();
            });

            jQuery2('body').on('click', function() {
                if (jQuery2('.search-btn').hasClass('show-search-icon')) {
                    jQuery2('.search-btn').removeClass("show-search-icon");
                    jQuery2('.search-box').fadeOut(300);
                }
            });
        }
    }

    var handleMenu = function() {
        jQuery2(".header .navbar-toggle").click(function () {
            if (jQuery2(".header .navbar-collapse").hasClass("open")) {
                jQuery2(".header .navbar-collapse").slideDown(300)
                .removeClass("open");
            } else {             
                jQuery2(".header .navbar-collapse").slideDown(300)
                .addClass("open");
            }
        });
    }
    var handleSubMenuExt = function() {
        jQuery2(".header-navigation .dropdown").on("hover", function() {
            if (jQuery2(this).children(".header-navigation-content-ext").show()) {
                if (jQuery2(".header-navigation-content-ext").height()>=jQuery2(".header-navigation-description").height()) {
                    jQuery2(".header-navigation-description").css("height", jQuery2(".header-navigation-content-ext").height()+22);
                }
            }
        });        
    }

    var handleSidebarMenu = function () {
        jQuery2(".sidebar .dropdown a").click(function (event) {
            event.preventDefault();
            if (jQuery2(this).hasClass("collapsed") == false) {
                jQuery2(this).addClass("collapsed");
                jQuery2(this).siblings(".dropdown-menu").slideDown(300);
            } else {
                jQuery2(this).removeClass("collapsed");
                jQuery2(this).siblings(".dropdown-menu").slideUp(300);
            }
        });
    }

    function handleDifInits() { 
        jQuery2(".header .navbar-toggle span:nth-child(2)").addClass("short-icon-bar");
        jQuery2(".header .navbar-toggle span:nth-child(4)").addClass("short-icon-bar");
    }

    function handleUniform() {
        if (!jQuery2().uniform) {
            return;
        }
        var test = jQuery2("input[type=checkbox]:not(.toggle), input[type=radio]:not(.toggle, .star)");
        if (test.size() > 0) {
            test.each(function () {
                    if (jQuery2(this).parents(".checker").size() == 0) {
                        jQuery2(this).show();
                        jQuery2(this).uniform();
                    }
                });
        }
    }

    var handleFancybox = function () {
        if (!jQuery.fancybox) {
            return;
        }
        
        jQuery2(".fancybox-fast-view").fancybox();

        if (jQuery2(".fancybox-button").size() > 0) {            
            jQuery2(".fancybox-button").fancybox({
                groupAttr: 'data-rel',
                prevEffect: 'none',
                nextEffect: 'none',
                closeBtn: true,
                helpers: {
                    title: {
                        type: 'inside'
                    }
                }
            });

            jQuery2('.fancybox-video').fancybox({
                type: 'iframe'
            });
        }
    }

    // Handles Bootstrap Accordions.
    var handleAccordions = function () {
       
        jQuery2('body').on('shown.bs.collapse', '.accordion.scrollable', function (e) {
            Layout.scrollTo(jQuery2(e.target), -100);
        });
        
    }

    // Handles Bootstrap Tabs.
    var handleTabs = function () {
        // fix content height on tab click
        jQuery2('body').on('shown.bs.tab', '.nav.nav-tabs', function () {
            handleSidebarAndContentHeight();
        });

        //activate tab if tab id provided in the URL
        if (location.hash) {
            var tabid = location.hash.substr(1);
            jQuery2('a[href="#' + tabid + '"]').click();
        }
    }

    var handleMobiToggler = function () {
        jQuery2(".mobi-toggler").on("click", function(event) {
            event.preventDefault();//the default action of the event will not be triggered
            
            jQuery2(".header").toggleClass("menuOpened");
            jQuery2(".header").find(".header-navigation").toggle(300);
        });
    }

    var handleTheme = function () {
    
        var panel = jQuery2('.color-panel');
    
        // handle theme colors
        var setColor = function (color) {
            jQuery2('#style-color').attr("href", "../../assets/frontend/layout/css/themes/" + color + ".css");
            jQuery2('.corporate .site-logo img').attr("src", "../../assets/frontend/layout/img/logos/logo-corp-" + color + ".png");
            jQuery2('.ecommerce .site-logo img').attr("src", "../../assets/frontend/layout/img/logos/logo-shop-" + color + ".png");
        }

        jQuery2('.icon-color', panel).click(function () {
            jQuery2('.color-mode').show();
            jQuery2('.icon-color-close').show();
        });

        jQuery2('.icon-color-close', panel).click(function () {
            jQuery2('.color-mode').hide();
            jQuery2('.icon-color-close').hide();
        });

        jQuery2('li', panel).click(function () {
            var color = jQuery2(this).attr("data-style");
            setColor(color);
            jQuery2('.inline li', panel).removeClass("current");
            jQuery2(this).addClass("current");
        });
    }
	

	
    return {
        init: function () {
            // init core variables
            handleTheme();
            handleInit();
            handleResponsiveOnResize();
            handleIEFixes();
            handleSearch();
            handleFancybox();
            handleDifInits();
            handleSidebarMenu();
            handleAccordions();
            handleMenu();
            handleScrollers();
            handleSubMenuExt();
            handleMobiToggler();
        },

        initUniform: function (els) {
            if (els) {
                jQuery2(els).each(function () {
                        if (jQuery2(this).parents(".checker").size() == 0) {
                            jQuery2(this).show();
                            jQuery2(this).uniform();
                        }
                    });
            } else {
                handleUniform();
            }
        },

        initTwitter: function () {
            !function(d,s,id){
                var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}
            }(document,"script","twitter-wjs");
        },

	    fixEditMode: function () {
			if(typeof Sitecore !== "undefined" && typeof Sitecore.PageModes !== "undefined" && typeof Sitecore.PageModes.PageEditor !== "undefined") {
				jQuery2("#DeviceId").PatchLink({
					property: "sc_device",
					value: jQuery2("#DeviceId").val()
				});
				jQuery2("#scCapabilities").PatchLink({
					property: "sc_mode",
					value: "edit"
				});
			}	
		},		
		
        initTouchspin: function () {
            jQuery2(".product-quantity .form-control").TouchSpin({
                buttondown_class: "btn quantity-down",
                buttonup_class: "btn quantity-up"
            });
            jQuery2(".quantity-down").html("<i class='fa fa-angle-down'></i>");
            jQuery2(".quantity-up").html("<i class='fa fa-angle-up'></i>");
        },

        initFixHeaderWithPreHeader: function () {
            jQuery2(window).scroll(function() {                
                if (jQuery2(window).scrollTop()>37){
                    jQuery2("body").addClass("page-header-fixed");
                }
                else {
                    jQuery2("body").removeClass("page-header-fixed");
                }
            });
        },

        initNavScrolling: function () {
            function NavScrolling () {
                if (jQuery2(window).scrollTop()>60){
                    jQuery2(".header").addClass("reduce-header");
                }
                else {
                    jQuery2(".header").removeClass("reduce-header");
                }
            }
            
            NavScrolling();
            
            jQuery2(window).scroll(function() {
                NavScrolling ();
            });
        },

        initOWL: function () {
            jQuery2(".owl-carousel6-brands").owlCarousel({
                pagination: false,
                navigation: true,
                items: 6,
                addClassActive: true,
                itemsCustom : [
                    [0, 1],
                    [320, 1],
                    [480, 2],
                    [700, 3],
                    [975, 5],
                    [1200, 6],
                    [1400, 6],
                    [1600, 6]
                ],
            });

            jQuery2(".owl-carousel5").owlCarousel({
                pagination: false,
                navigation: true,
                items: 5,
                addClassActive: true,
                itemsCustom : [
                    [0, 1],
                    [320, 1],
                    [480, 2],
                    [660, 2],
                    [700, 3],
                    [768, 3],
                    [992, 4],
                    [1024, 4],
                    [1200, 5],
                    [1400, 5],
                    [1600, 5]
                ],
            });

            jQuery2(".owl-carousel4").owlCarousel({
                pagination: false,
                navigation: true,
                items: 4,
                addClassActive: true,
            });

            jQuery2(".owl-carousel3").owlCarousel({
                pagination: false,
                navigation: true,
                items: 3,
                addClassActive: true,
                itemsCustom : [
                    [0, 1],
                    [320, 1],
                    [480, 2],
                    [700, 3],
                    [768, 2],
                    [1024, 3],
                    [1200, 3],
                    [1400, 3],
                    [1600, 3]
                ],
            });

            jQuery2(".owl-carousel2").owlCarousel({
                pagination: false,
                navigation: true,
                items: 2,
                addClassActive: true,
                itemsCustom : [
                    [0, 1],
                    [320, 1],
                    [480, 2],
                    [700, 3],
                    [975, 2],
                    [1200, 2],
                    [1400, 2],
                    [1600, 2]
                ],
            });
        },

        initImageZoom: function () {
            jQuery2('.product-main-image').zoom({url: jQuery2('.product-main-image img').attr('data-BigImgSrc')});
        },

        initSliderRange: function () {
            jQuery2( "#slider-range" ).slider({
              range: true,
              min: 0,
              max: 500,
              values: [ 50, 250 ],
              slide: function( event, ui ) {
                jQuery2( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
              }
            });
            jQuery2( "#amount" ).val( "$" + jQuery2( "#slider-range" ).slider( "values", 0 ) +
            " - $" + jQuery2( "#slider-range" ).slider( "values", 1 ) );
        },

        // wrapper function to scroll(focus) to an element
        scrollTo: function (el, offeset) {
            var pos = (el && el.size() > 0) ? el.offset().top : 0;
            if (el) {
                if (jQuery2('body').hasClass('page-header-fixed')) {
                    pos = pos - jQuery2('.header').height(); 
                }            
                pos = pos + (offeset ? offeset : -1 * el.height());
            }

            jQuery2('html,body').animate({
                scrollTop: pos
            }, 'slow');
        },

        //public function to add callback a function which will be called on window resize
        addResponsiveHandler: function (func) {
            responsiveHandlers.push(func);
        },

        scrollTop: function () {
            App.scrollTo();
        },

        gridOption1: function () {
            jQuery2(function(){
                jQuery2('.grid-v1').mixitup();
            });    
        }

    };
}();