﻿using Sitecore.Analytics.Automation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Officecore.Dev.SaveActions
{
    class DummyAutomationAction : IAutomationAction
    {
        public AutomationActionResult Execute(AutomationActionContext context)
        {
            return AutomationActionResult.Continue;
        }
    }
}
