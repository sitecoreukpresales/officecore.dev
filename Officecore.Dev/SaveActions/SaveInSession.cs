﻿using System.Web;

using Sitecore.Data;
using Sitecore.WFFM.Actions.Base;
using Sitecore.WFFM.Abstractions.Actions;

namespace Officecore.Dev.SaveActions
{
    public class SaveInSession : WffmSaveAction
    {
        public override void Execute(ID formid, AdaptedResultList fields, ActionCallContext actionCallContext = null, params object[] data)
        {
            HttpContext.Current.Session["oc_FormFields"] = fields;
        }
    }
}
