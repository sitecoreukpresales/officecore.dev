﻿using System.Web;
using System;
using Sitecore.Data;
using Salesforce.Common;
using Salesforce.Force;
using System.Threading.Tasks;
using System.Globalization;

using System.Threading;
using Sitecore.WFFM.Actions.Base;
using Sitecore.WFFM.Abstractions.Actions;

namespace Officecore.Dev.SaveActions
{
    public class CreateCRMCase : WffmSaveAction
    {
        private static string consumerKey = Sitecore.Configuration.Settings.GetSetting("sfconsumerKey");
        private static string consumerSecret = Sitecore.Configuration.Settings.GetSetting("sfconsumerSecret");
        private static string username = Sitecore.Configuration.Settings.GetSetting("sfuserName");
        private static string password = Sitecore.Configuration.Settings.GetSetting("sfpassword");
        private static string token = Sitecore.Configuration.Settings.GetSetting("sfsecurityToken");
        private static string caseId = string.Empty;
        private static AuthenticationClient auth = new AuthenticationClient();

        private static Case sfcase = new Case();


        public override void Execute(ID formId, AdaptedResultList adaptedFields, ActionCallContext actionCallContext = null, params object[] data)
        {
            try
            {
                Run(adaptedFields);
                HttpContext.Current.Session["caseID"] = caseId;
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("Error Is Salesforce Save Case Action", ex, this);
            }
        }



        private static async Task Run(AdaptedResultList fields)
        {
            var t = auth.UsernamePasswordAsync(consumerKey, consumerSecret, username, password + token);

            var num = 1;
            while (!t.IsCompleted)
            {
                if (num == 20)
                {
                    Sitecore.Diagnostics.Log.Error("Failed to authticate to Salesforce in time", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                    return;
                }
                Thread.Sleep(200);
                num++;
            }

            if (string.IsNullOrEmpty(auth.AccessToken))
            {
                Sitecore.Diagnostics.Log.Error("Failed to authticate to Salesforce", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                return;
            }

            CreateCase(fields);
        }


        public static string CreateCase(AdaptedResultList fields)
        {

            var client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);
            sfcase = new Case() { Subject = fields.GetEntryByName("Subject").Value, Description = fields.GetEntryByName("Description").Value, SuppliedEmail = fields.GetEntryByName("E-Mail").Value };


            var t = client.CreateAsync("Case", sfcase);
            int num = 1;
            while (!t.IsCompleted)
            {
                Thread.Sleep(500);
                num++;
                if (num == 15)
                {
                    return "";
                }
            }

            sfcase.Id = t.Result;

            if (sfcase.Id == null)
            {
                Sitecore.Diagnostics.Log.Error("Unable to create contact in Salesforce", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                return "";
            }

            caseId = sfcase.Id;
            return sfcase.Id;
        }

        private class Case
        {
            public string Subject { get; set; }
            public string Description { get; set; }
            public string SuppliedEmail { get; set; }

            public string Id { get; set; }
        }


    }
}
