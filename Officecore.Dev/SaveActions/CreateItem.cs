﻿using Sitecore.WFFM.Actions.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore.Data;
using Sitecore.SecurityModel;
using Sitecore.WFFM.Abstractions.Actions;
using Sitecore.Workflows;
using Sitecore.Form.Core.Configuration;
using Sitecore.Diagnostics;
using Sitecore.Data.Items;

using System.Collections.Specialized;
using Sitecore;
using System.Collections;
using Sitecore.Form.Core.Utility;
using Sitecore.Forms.Core.Data;

namespace Officecore.Dev.SaveActions
{
    public class CreateItem : WffmSaveAction
    {

        public bool CheckSecurity { get; set; }

        public string Mapping { get; set; }

        public string Destination { get; set; }

        public string Template { get; set; }

        public CreateItem()
        {
            this.CheckSecurity = false;
        }

        public override void Execute(ID formId, AdaptedResultList adaptedFields, ActionCallContext actionCallContext = null, params object[] data)
        {
            SecurityDisabler securityDisabler = (SecurityDisabler)null;
            if (!this.CheckSecurity)
                securityDisabler = new SecurityDisabler();
            try
            {
                this.CreateItemByFields(formId, adaptedFields);
            }
            finally
            {
                if (securityDisabler != null)
                    securityDisabler.Dispose();
            }
        }

        protected virtual void CreateItemByFields(ID formid, AdaptedResultList fields)
        {
            WorkflowContextStateSwitcher contextStateSwitcher = new WorkflowContextStateSwitcher(WorkflowContextState.Enabled);
            if (StaticSettings.MasterDatabase == null)
                Log.Warn("The Create Item action : the master database is unavailable", (object)this);
            TemplateItem template = StaticSettings.MasterDatabase.GetTemplate(this.Template);
            Error.AssertNotNull((object)template, "Template Not Found");
            Item destination = StaticSettings.MasterDatabase.GetItem(this.Destination);
            Error.AssertNotNull((object)destination, "Item Not Found");
            DateTime today = DateTime.Today;
            string str1 = today.Year.ToString("0000");
            today = DateTime.Today;
            string str2 = today.Month.ToString("00");
            today = DateTime.Today;
            string str3 = today.Day.ToString("00");
            string str6 = today.Minute.ToString() + today.Second.ToString() + today.Millisecond.ToString();
            string str4 = Enumerable.ToList<AdaptedControlResult>((IEnumerable<AdaptedControlResult>)fields)[1].Value;
            Item obj = Sitecore.Data.Managers.ItemManager.CreateItem(str1 + str2 + str3 + str4 + str6, destination, template.ID);
            NameValueCollection nameValueCollection = StringUtil.ParseNameValueCollection(this.Mapping, '|', '=');
            obj.Editing.BeginEdit();
            IEnumerator enumerator = fields.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    AdaptedControlResult adaptedControlResult = (AdaptedControlResult)enumerator.Current;
                    if (nameValueCollection[((ControlResult)adaptedControlResult).FieldID] != null)
                    {
                        string index = nameValueCollection[((ControlResult)adaptedControlResult).FieldID];
                        if (obj.Fields[index] != null)
                        {
                            string str5 = string.Join("|", new List<string>(FieldReflectionUtil.GetAdaptedListValue(new FieldItem(StaticSettings.ContextDatabase.GetItem(((ControlResult)adaptedControlResult).FieldID)), adaptedControlResult.Value, false)).ToArray());
                            obj.Fields[index].Value = str5;
                            if (index == Sitecore.FieldIDs.DisplayName.ToString())
                                obj.Name = Sitecore.Data.Items.ItemUtil.ProposeValidItemName(adaptedControlResult.Value);
                        }
                        else
                            Log.Warn(string.Format("The Create Item action : the template does not contain field: {0}", (object)index), (object)this);
                    }
                }
            }
            finally
            {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable != null)
                    disposable.Dispose();
            }
            obj.Editing.EndEdit();
        }

    }
}
