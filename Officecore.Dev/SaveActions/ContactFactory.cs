﻿using Sitecore.Analytics;
using Sitecore.Analytics.Data;
using Sitecore.Analytics.DataAccess;
using Sitecore.Analytics.Tracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Officecore.Dev.SaveActions
{
    public class ContactFactory
    {
        ContactRepository contactRepository = Sitecore.Configuration.Factory.CreateObject("tracking/contactRepository", true) as ContactRepository;
        ContactManager contactManager = Sitecore.Configuration.Factory.CreateObject("tracking/contactManager", true) as ContactManager;

        public Contact GetContact(string emailAddress)
        {

            if (string.IsNullOrEmpty(emailAddress))
                return Tracker.Current.Session.Contact;

            if (IsContactInSession(emailAddress))
                return Tracker.Current.Session.Contact;



            var contact = contactRepository.LoadContactReadOnly(emailAddress);

            Contact matchedContact;

            if (contact != null)
            {
                LockAttemptResult<Contact> lockResult = contactManager.TryLoadContact(contact.ContactId);

                switch (lockResult.Status)
                {
                    case LockAttemptStatus.Success:
                        Contact lockedContact = lockResult.Object;
                        lockedContact.ContactSaveMode = Sitecore.Analytics.Model.ContactSaveMode.AlwaysSave;
                        matchedContact = lockedContact;
                        break;
                    case LockAttemptStatus.NotFound:
                        matchedContact = Tracker.Current.Session.Contact;
                        break;
                    default:
                        throw new Exception(this.GetType() + " Contact could not be locked - " + emailAddress);
                }
            }
            else
            {
                matchedContact = Tracker.Current.Session.Contact;
            }

            // If the matched Contact is not as the same as the Contact Sitecore loaded into session
            // then we need to call .Identify() - more on this later
            if (!matchedContact.ContactId.Equals(Tracker.Current.Session.Contact.ContactId))
                Tracker.Current.Session.Identify(emailAddress);

            return matchedContact;
        }

        private bool IsContactInSession(string emailAddress)
        {
            var tracker = Tracker.Current;

            if (tracker != null &&
              tracker.IsActive &&
              tracker.Session != null &&
              tracker.Session.Contact != null &&
              tracker.Session.Contact.Identifiers != null &&
              tracker.Session.Contact.Identifiers.Identifier != null &&
              tracker.Session.Contact.Identifiers.Identifier.Equals(emailAddress, StringComparison.InvariantCultureIgnoreCase))
                return true;

            return false;
        }
    }
}
