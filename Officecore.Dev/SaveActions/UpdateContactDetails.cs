﻿using Sitecore.Analytics.Data;
using Sitecore.Analytics.Tracking;
using Sitecore.Data;
using Sitecore.Diagnostics;
using Sitecore.WFFM.Abstractions;
using Sitecore.WFFM.Abstractions.Actions;
using Sitecore.WFFM.Abstractions.Analytics;
using Sitecore.WFFM.Abstractions.Shared;
using Sitecore.WFFM.Actions.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web.Script.Serialization;

namespace Officecore.Dev.SaveActions
{
    [Required("IsXdbTrackerEnabled", true)]
    public class UpdateContactDetails : WffmSaveAction
    {
        private readonly IAnalyticsTracker analyticsTracker;

        private readonly IAuthentificationManager authentificationManager;

        private readonly ILogger logger;

        private readonly IFacetFactory facetFactory;

        private readonly IContactManager contactManager;

        private string email;

        public string Mapping
        {
            get;
            set;
        }

        public UpdateContactDetails(IAnalyticsTracker analyticsTracker, IAuthentificationManager authentificationManager, ILogger logger, IFacetFactory facetFactory, IContactManager contactManager)
        {
            Assert.IsNotNull(analyticsTracker, "analyticsTracker");
            Assert.IsNotNull(authentificationManager, "authentificationManager");
            Assert.IsNotNull(logger, "logger");
            Assert.IsNotNull(facetFactory, "facetFactory");
            Assert.IsNotNull(contactManager, "contactManager");
            this.analyticsTracker = analyticsTracker;
            this.authentificationManager = authentificationManager;
            this.logger = logger;
            this.facetFactory = facetFactory;
            this.contactManager = contactManager;
        }

        public override void Execute(ID formId, AdaptedResultList fields, ActionCallContext actionCallContext = null, params object[] data)
        {
            UpdateContact(fields);
        }

        public IEnumerable<FacetNode> ParseMapping(string mapping, AdaptedResultList adaptedFieldResultList)
        {
            Assert.ArgumentNotNullOrEmpty(mapping, "mapping");
            Assert.ArgumentNotNull(adaptedFieldResultList, "adaptedFieldResultList");
            object[] objArray = (object[])(new JavaScriptSerializer()).Deserialize(mapping, typeof(object));
            return (
                from Dictionary<string, object> item in objArray
                let itemValue = item["value"].ToString()
                let itemId = (!item.ContainsKey("id") || item["id"] == null ? "Preferred" : item["id"].ToString())
                let value = adaptedFieldResultList.GetValueByFieldID(ID.Parse(item["key"].ToString()))
                where !string.IsNullOrEmpty(value)
                select new FacetNode(itemId, itemValue, value)).ToList<FacetNode>();
        }

        protected void UpdateContact(AdaptedResultList fields)
        {
            Assert.ArgumentNotNull(fields, "adaptedFields");
            Assert.IsNotNullOrEmpty(this.Mapping, "Empty mapping xml.");
            Assert.IsNotNull(this.analyticsTracker.CurrentContact, "Tracker.Current.Contact");

            IEnumerable<FacetNode> facetNodes = this.ParseMapping(this.Mapping, fields);
            IContactFacetFactory contactFacetFactory = this.facetFactory.GetContactFacetFactory();
            Contact currentContact = this.analyticsTracker.CurrentContact;
            currentContact.Identifiers.IdentificationLevel = Sitecore.Analytics.Model.ContactIdentificationLevel.Known;


            foreach (FacetNode facetNode in facetNodes)
            {
                if (facetNode.Path == "Emails/Preferred" || facetNode.Path == "Emails/Entries/SmtpAddress")
                {
                    email = facetNode.Value;
                }
                contactFacetFactory.SetFacetValue(this.analyticsTracker.CurrentContact, facetNode.Key, facetNode.Path, facetNode.Value, true);
            }
            this.contactManager.SaveAndReleaseContactToXdb(currentContact);

            var contactFactory = new ContactFactory();
            Contact contact = contactFactory.GetContact(email);

            if (contact != null)
            {

                ContactRepository contactRepository = Sitecore.Configuration.Factory.CreateObject("tracking/contactRepository", true) as ContactRepository;

                // The data will be transferred from the dyingContact to the survivingContact
                contactRepository.MergeContacts(currentContact, contact);
                Sitecore.Analytics.Tracker.Current.Session.Identify(email);
            }


        }







        /*
                protected virtual void UpdateContact(AdaptedResultList fields)
                {
                    foreach (KeyValuePair<string, string> keyValuePair in dictionary)
                    {
                        // Helped by http://blog.horizontalintegration.com/2014/11/05/sitecore-8-how-to-programatically-associate-visitor-data-into-contact-card/
                        if (keyValuePair.Key == "Emails/Preferred")
                        {
                            var emailFacet = Tracker.Current.Contact.GetFacet<IContactEmailAddresses>("Emails");

                            if (!emailFacet.Entries.Contains("Work Email"))
                            {
                                IEmailAddress email = emailFacet.Entries.Create("Work Email");
                                email.SmtpAddress = keyValuePair.Value;
                                emailFacet.Preferred = "Work Email";
                            }
                        }
                        else if (keyValuePair.Key == "Phone Numbers/Preferred")
                        {
                            var phoneFacet = Tracker.Current.Contact.GetFacet<IContactPhoneNumbers>("Phone Numbers");
                            if (!phoneFacet.Entries.Contains("Work Phone"))
                            {
                                IPhoneNumber workPhone = phoneFacet.Entries.Create("Work Phone");
                                workPhone.Number = keyValuePair.Value;
                                phoneFacet.Preferred = "Work Phone";
                            }
                        }
                        else
                        {
                            contactFacetFactory.SetFacetValue(Tracker.Current.Contact, keyValuePair.Key, null, keyValuePair.Value, true);
                        }
                    }

                    contactManager.SaveAndReleaseContact(contact);
                    
          }
*/

    }
}
