﻿using Sitecore.Analytics.Tracking;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.ListManagement.Configuration;
using Sitecore.SecurityModel;
using Sitecore.WFFM.Abstractions;
using Sitecore.WFFM.Abstractions.Actions;
using Sitecore.WFFM.Abstractions.Analytics;
using Sitecore.WFFM.Abstractions.Shared;
using Sitecore.WFFM.Actions.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Officecore.Dev.SaveActions
{
    [Required("IsXdbEnabled", true)]
    [Required("IsXdbTrackerEnabled", true)]
    public class AddContactToContactList : WffmSaveAction
    {
        private readonly IAnalyticsTracker analyticsTracker;

        private readonly IContactRepository contactRepository;

        public string ContactsLists
        {
            get;
            set;
        }

        public string ExecuteWhen
        {
            get;
            set;
        }

        public AddContactToContactList(IAnalyticsTracker analyticsTracker, IContactRepository contactRepository)
        {
            Assert.IsNotNull(analyticsTracker, "analyticsTracker");
            Assert.IsNotNull(contactRepository, "contactRepository");
            this.analyticsTracker = analyticsTracker;
            this.contactRepository = contactRepository;
        }

        public override void Execute(ID formId, AdaptedResultList adaptedFields, ActionCallContext actionCallContext = null, params object[] data)
        {
            Assert.ArgumentNotNull(adaptedFields, "adaptedFields");
            Assert.IsNotNullOrEmpty(this.ContactsLists, "Empty contact list.");
            Assert.IsNotNull(this.analyticsTracker.CurrentContact, "Tracker.Current.Contact");
            if (!adaptedFields.IsTrueStatement(this.ExecuteWhen))
            {
                return;
            }
            string contactsLists = this.ContactsLists;
            char[] chrArray = new char[] { ',' };
            List<string> list = (
                from x in contactsLists.Split(chrArray)
                select ID.Parse(x).ToString()).ToList<string>();
            using (SecurityDisabler securityDisabler = new SecurityDisabler())
            {
                Contact currentContact = this.analyticsTracker.CurrentContact;
                foreach (string str in list)
                {
                    currentContact.Tags.Set("ContactLists", str);
                    Item item = Factory.GetDatabase(ListManagementSettings.Database).GetItem(new ID(str));
                    if (item != null)
                    {
                        int c = 0;
                        if (int.TryParse(item["Recipients"], out c))
                        {
                            item.Editing.BeginEdit();
                            item["Recipients"] = (c + 1).ToString();
                            item.Editing.EndEdit();
                        }
                    }

                }
                this.contactRepository.SaveContact(currentContact, true, null, new TimeSpan?(new TimeSpan((long)1000)));



            }
        }
    }
}
